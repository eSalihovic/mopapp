//
//  MainVC.swift
//  MOPApp
//
//  Created by Edin Salihovic on 3/13/19.
//  Copyright © 2019 Edin Salihovic. All rights reserved.
//

import UIKit

class MainVC: UIViewController {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var introView: UIView!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var currentPage = 1
    var searchTextCount = 1
    var contributors = [[Contributor]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
    }
    
    private func setupNavBar() {
        searchController.searchBar.tintColor = .white
        searchController.searchBar.barStyle = .black
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = searchRepositories
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == profileVC, let profileVC = segue.destination as? ProfileVC, let userUrl = sender as? String {
            profileVC.userUrl = userUrl
        }
        else if segue.identifier == contributorsVC, let contributorsVC = segue.destination as? ContributorsVC, let contributors = sender as? [Contributor] {
            contributorsVC.contributors = contributors
        }
    }
}

extension MainVC: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text, !text.isEmpty else { return }
        
        if searchTextCount != text.count {
        RepoService.shared.items.removeAll()
        contributors.removeAll()
            searchTextCount = text.count
        }
        
        RepoService.shared.getRepos(text, page: currentPage) { (success) in
            guard success else { return }
            self.introView.isHidden = true
            for item in RepoService.shared.items {
                RepoService.shared.getContributors(from: item.contributorsUrl ?? emptyString, completion: { (result) in
                    guard let contributors = result else {
                        self.introView.isHidden = false
                        return
                    }
                    self.contributors.append(contributors)
                    self.tableView.reloadData()
                })
            }
        }
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isSearching() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
}

extension MainVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return RepoService.shared.items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.contentOffset.y >= (tableView.contentSize.height - tableView.frame.size.height) && currentPage != 0 {
            currentPage += 1
            updateSearchResults(for: searchController)
        }
        
        let item = RepoService.shared.items[indexPath.section]
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: repositoryCell, for: indexPath) as? RepositoryCell else { return RepositoryCell() }
            cell.configureCell(with: item)
            return cell
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: contributorsCell, for: indexPath) as? ContributorsCell else { return ContributorsCell() }
            guard indexPath.section < self.contributors.count else { return ContributorsCell() }
            let contributors = self.contributors[indexPath.section]
            let stars = item.stargazersCount ?? 0
            cell.configureCell(with: contributors, stars)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let userUrl = RepoService.shared.items[indexPath.section].owner?.url
            performSegue(withIdentifier: profileVC, sender: userUrl)
        }
        else {
            performSegue(withIdentifier: contributorsVC, sender: contributors[indexPath.section])
        }
    }
}
