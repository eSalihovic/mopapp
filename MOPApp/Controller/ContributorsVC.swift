//
//  ContributorsVC.swift
//  MOPApp
//
//  Created by Edin Salihovic on 3/14/19.
//  Copyright © 2019 Edin Salihovic. All rights reserved.
//

import UIKit

class ContributorsVC: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var contributors = [Contributor]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
    }
    
    private func setupNavBar() {
        navigationItem.largeTitleDisplayMode = .never
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == profileVC, let profileVC = segue.destination as? ProfileVC, let userUrl = sender as? String else { return }
        profileVC.userUrl = userUrl
    }
}

extension ContributorsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contributors.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: contributorCell, for: indexPath) as? ContributorCell else { return ContributorCell() }
        let contributor = contributors[indexPath.row]
        cell.configureCell(with: contributor)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userUrl = contributors[indexPath.row].url
        performSegue(withIdentifier: profileVC, sender: userUrl)
    }
}
