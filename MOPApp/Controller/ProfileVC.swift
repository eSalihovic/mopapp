//
//  ProfileVC.swift
//  MOPApp
//
//  Created by Edin Salihovic on 3/13/19.
//  Copyright © 2019 Edin Salihovic. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass

// State
private enum State {
    case closed
    case open
}

extension State {
    var opposite: State {
        switch self {
        case .open: return .closed
        case .closed: return .open
        }
    }
}

class ProfileVC: UIViewController {
    
    @IBOutlet var userAvatar: CircleImage!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var userLabel: UILabel!
    @IBOutlet var followersLabel: UILabel!
    @IBOutlet var publicReposLabel: UILabel!
    @IBOutlet var followingLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    
    var userUrl: String?
    var contributors = [Contributor]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getUserData()
    }
    
    private func setupNavBar() {
        navigationItem.largeTitleDisplayMode = .never
    }
    
    private func getUserData() {
        guard let url = userUrl else { return }
        UserService.shared.getUser(url) { (success) in
            guard let user = UserService.shared.user else { return }
            self.userAvatar.sd_setImage(with: URL(string: user.avatarUrl ?? emptyString), placeholderImage: UIImage(named: avatar))
            self.navigationItem.title = user.name ?? emptyString
            self.nameLabel.text = user.name ?? emptyString
            self.locationLabel.text = user.location ?? emptyString
            self.userLabel.text = "@\(user.login ?? emptyString)"
            self.followersLabel.text = String(user.followers ?? 0)
            self.followingLabel.text = String(user.following ?? 0)
            self.publicReposLabel.text = String(user.publicRepos ?? 0)
            guard let reposUrl = user.reposUrl else { return }
            UserService.shared.getRepos(reposUrl, completion: { (sucess) in
                guard success else { return }
                self.tableView.reloadData()
            })
        }
    }
}

extension ProfileVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return UserService.shared.repos.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = RepoService.shared.items[indexPath.section]
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: repositoryCell, for: indexPath) as? RepositoryCell else { return RepositoryCell() }
            cell.configureCell(with: item)
            cell.roundCorners([UIRectCorner.topLeft, UIRectCorner.topRight], radius: 7)
            return cell
            
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: contributorsCell, for: indexPath) as? ContributorsCell else { return ContributorsCell() }
            guard indexPath.section < self.contributors.count else { return ContributorsCell() }
            let stars = item.stargazersCount ?? 0
            cell.configureCell(with: contributors, stars)
            cell.roundCorners([UIRectCorner.bottomLeft, UIRectCorner.bottomRight], radius: 5)
            return cell
        }
    }
}
