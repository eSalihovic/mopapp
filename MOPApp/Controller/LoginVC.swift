//
//  LoginVC.swift
//  MOPApp
//
//  Created by Edin Salihovic on 3/13/19.
//  Copyright © 2019 Edin Salihovic. All rights reserved.
//

import UIKit
import GoogleSignIn

class LoginVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
        configureUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        userLoginCheck()
    }
    
    private func userLoginCheck()  {
        guard AuthService.shared.isLoggedIn else { return }
        performSegue(withIdentifier: mainVC, sender: nil)
    }
    
    private func configureUI() {
        let googleSignInButton = GIDSignInButton(frame: CGRect(x: 0, y: 0, width: 150, height: 70))
        googleSignInButton.center = view.center
        view.addSubview(googleSignInButton)
    }
}

extension LoginVC: GIDSignInUIDelegate, GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            let alert = UIAlertController(title: error.localizedDescription, message: nil, preferredStyle: .alert)
            let cancel = UIAlertAction(title: OK, style: .cancel, handler: nil)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }
        else {
            AuthService.shared.isLoggedIn = true
            performSegue(withIdentifier: mainVC, sender: nil)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        AuthService.shared.isLoggedIn = false
    }
}
