//
//  AuthService.swift
//  MOPApp
//
//  Created by Edin Salihovic on 3/14/19.
//  Copyright © 2019 Edin Salihovic. All rights reserved.
//

import Foundation

class AuthService {
    
    static let shared = AuthService()
    
    let defaults = UserDefaults.standard
    
    var isLoggedIn: Bool {
        get {
            return defaults.bool(forKey: loggedInKey)
        }
        set {
            defaults.set(newValue, forKey: loggedInKey)
        }
    }
}
