//
//  UserService.swift
//  MOPApp
//
//  Created by Edin Salihovic on 3/18/19.
//  Copyright © 2019 Edin Salihovic. All rights reserved.
//

import Foundation
import Alamofire

class UserService {
    
    static let shared = UserService()
    
    var user: User?
    var repos = [Item]()
    
    func getUser(_ url: String, completion: @escaping CompletionHandler) {
        Spinner.start()
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {
                    Spinner.stop()
                    completion(false)
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let user = try decoder.decode(User.self, from: data)
                    self.user = user
                    Spinner.stop()
                    completion(true)
                }
                catch let jsonError {
                    Spinner.stop()
                    completion(false)
                    debugPrint("Error serializing json", jsonError)
                }
            }
            else {
                Spinner.stop()
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
    
    func getRepos(_ url: String, completion: @escaping CompletionHandler) {
        Spinner.start()
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {
                    Spinner.stop()
                    completion(false)
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let repos = try decoder.decode([Item].self, from: data)
                    self.repos = repos
                    Spinner.stop()
                    completion(true)
                }
                catch let jsonError {
                    Spinner.stop()
                    completion(false)
                    debugPrint("Error serializing json", jsonError)
                }
            }
            else {
                Spinner.stop()
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
}
