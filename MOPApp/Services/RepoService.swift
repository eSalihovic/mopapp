//
//  RepoService.swift
//  MOPApp
//
//  Created by Edin Salihovic on 3/14/19.
//  Copyright © 2019 Edin Salihovic. All rights reserved.
//

import Foundation
import Alamofire

struct Message: Codable {
    let message: String?
}

class RepoService {
    
    static let shared = RepoService()
    
    var justOneAlert = true
    var items = [Item]()
    
    func getRepos(_ query: String, page: Int, perPage: Int = perPage, completion: @escaping CompletionHandler) {
        Spinner.start()
        let url = String(format: getRepositories, query, page, perPage)
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {
                    Spinner.stop()
                    completion(false)
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let repo = try decoder.decode(Repo.self, from: data)
                    guard let items = repo.items else {
                        Spinner.stop()
                        completion(false)
                        return
                    }
                    for item in items {
                        self.items.append(item)
                    }
                    Spinner.stop()
                    completion(true)
                }
                catch let jsonError {
                    Spinner.stop()
                    completion(false)
                    debugPrint("Error serializing json", jsonError)
                }
            }
            else {
                Spinner.stop()
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
    
     func getContributors(from url: String, completion: @escaping (_ result: [Contributor]?) -> Void) {
        Spinner.start()
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {
                    Spinner.stop()
                    completion(nil)
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let contributors = try decoder.decode([Contributor].self, from: data)
                    Spinner.stop()
                    completion(contributors)
                }
                catch let jsonError {
                    Spinner.stop()
                    self.errorHandler(data)
                    completion(nil)
                    debugPrint("Error serializing json", jsonError)
                }
            }
            else {
                completion(nil)
                debugPrint(response.result.error as Any)
            }
        }
    }
    
    private func errorHandler(_ data: Data) {
        guard justOneAlert else { return }
        do {
            let decoder = JSONDecoder()
            let message = try decoder.decode(Message.self, from: data)
            let alertController = UIAlertController(title: nil, message: message.message, preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: OK, style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindow.Level.alert + 1;
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
            justOneAlert = false
        }
        catch let jsonError {
            debugPrint("Error serializing json", jsonError)
        }
    }
}
