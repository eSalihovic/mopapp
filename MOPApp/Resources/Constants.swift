//
//  Constants.swift
//  MOPApp
//
//  Created by Edin Salihovic on 3/13/19.
//  Copyright © 2019 Edin Salihovic. All rights reserved.
//

import Foundation

typealias CompletionHandler = (_ success: Bool) -> ()

let perPage = 10
// URL Constants
let getRepositories = "https://api.github.com/search/repositories?q=%@&sort=stars&order=desc&page=%d&per_page=%d"

// Headers
let HEADER = [
    "Content-Type": "application/json; charset=utf-8"
]

// Segues
let mainVC                  = "toMainVC"
let profileVC               = "toProfileVC"
let contributorsVC          = "toContributorsVC"

// Cells
let repositoryCell          = "RepositoryCell"
let contributorsCell        = "ContributorsCell"
let contributorCell         = "ContributorCell"
let profileCell             = "ProfileCell"

// User Defaults
let loggedInKey             = "loggedIn"

// Strings
let emptyString             = ""
let OK                      = "Okay"
let searchRepositories      = "Search Repositories"

// Placeholders
let avatar                  = "avatar.pdf"
