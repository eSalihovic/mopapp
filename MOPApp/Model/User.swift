//
//  User.swift
//  MOPApp
//
//  Created by Edin Salihovic on 3/18/19.
//  Copyright © 2019 Edin Salihovic. All rights reserved.
//

import Foundation

struct User: Codable {
    let login: String?
    let avatarUrl: String?
    let reposUrl: String?
    let name: String?
    let location: String?
    let publicRepos: Int?
    let followers: Int?
    let following: Int?
}
