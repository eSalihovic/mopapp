//
//  Item.swift
//  MOPApp
//
//  Created by Edin Salihovic on 3/16/19.
//  Copyright © 2019 Edin Salihovic. All rights reserved.
//

import Foundation

struct Item: Codable {
    let id: Int?
    let name: String?
    let fullName: String?
    let owner: Owner?
    let description: String?
    let contributorsUrl: String?
    let stargazersCount: Int?
}
