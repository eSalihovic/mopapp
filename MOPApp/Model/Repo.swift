//
//  RepoModel.swift
//  MOPApp
//
//  Created by Edin Salihovic on 3/14/19.
//  Copyright © 2019 Edin Salihovic. All rights reserved.
//

import Foundation

struct Repo: Codable {
    let items: [Item]?
}
