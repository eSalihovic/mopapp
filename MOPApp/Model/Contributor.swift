//
//  Contributor.swift
//  MOPApp
//
//  Created by Edin Salihovic on 3/16/19.
//  Copyright © 2019 Edin Salihovic. All rights reserved.
//

import Foundation

struct Contributor: Codable {
    let login: String?
    let avatarUrl: String?
    let url: String?
}
