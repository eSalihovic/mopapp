//
//  CircleImage.swift
//  MOPApp
//
//  Created by Edin Salihovic on 3/13/19.
//  Copyright © 2019 Edin Salihovic. All rights reserved.
//

import UIKit

class CircleImage: UIImageView {
    
    override func awakeFromNib() {
        setupView()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupView()
    }
    
    func setupView() {
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
}
