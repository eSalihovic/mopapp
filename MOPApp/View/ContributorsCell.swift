//
//  ContributorsCell.swift
//  MOPApp
//
//  Created by Edin Salihovic on 3/14/19.
//  Copyright © 2019 Edin Salihovic. All rights reserved.
//

import UIKit
import SDWebImage

class ContributorsCell: UITableViewCell {
    
    @IBOutlet var contributors: [CircleImage]!
    @IBOutlet var contributorsCountLabel: UILabel!
    @IBOutlet var starsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        for contributor in contributors {
            contributor.image = nil
        }
        contributorsCountLabel.text = emptyString
        starsLabel.text = emptyString
    }
    
    func configureCell(with contribs: [Contributor], _ stars: Int) {
        guard !contribs.isEmpty else {
            for c in contributors {
                c.image = nil
            }
            return
        }
        
        for index in 0..<3 {
            contributors[index].sd_setImage(with: URL(string: contribs[index].avatarUrl ?? emptyString), placeholderImage: UIImage(named: avatar))
        }
        contributorsCountLabel.text = "+\(contribs.count - 3)"
        starsLabel.text = String(stars)
    }
}
