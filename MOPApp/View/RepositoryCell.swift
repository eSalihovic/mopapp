//
//  RepositoryCell.swift
//  MOPApp
//
//  Created by Edin Salihovic on 3/13/19.
//  Copyright © 2019 Edin Salihovic. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoryCell: UITableViewCell {
    
    @IBOutlet var userAvatar: CircleImage!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var fullNameLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        userAvatar.image = nil
        nameLabel.text = emptyString
        fullNameLabel.text = emptyString
        descriptionLabel.text = emptyString
    }
    
    func configureCell(with item: Item) {
        userAvatar.sd_setImage(with: URL(string: item.owner?.avatarUrl ?? emptyString), placeholderImage: UIImage(named: avatar))
        nameLabel.text = item.name
        fullNameLabel.text = item.fullName
        descriptionLabel.text = item.description
    }
}
