//
//  Spinner.swift
//  MOPApp
//
//  Created by Edin Salihovic on 3/15/19.
//  Copyright © 2019 Edin Salihovic. All rights reserved.
//

import UIKit

open class Spinner {
    
    internal static var spinner: UIActivityIndicatorView?
    
    public static var style: UIActivityIndicatorView.Style = .whiteLarge
    public static var baseBackColor = UIColor(white: 0, alpha: 0.1)
    public static var baseColor: UIColor = .white
    
    public static func start(style: UIActivityIndicatorView.Style = style, backColor: UIColor = baseBackColor, baseColor: UIColor = baseColor) {
        guard spinner == nil, let window = UIApplication.shared.keyWindow else { return }
        let frame = UIScreen.main.bounds
        spinner = UIActivityIndicatorView(frame: frame)
        spinner!.backgroundColor = backColor
        spinner!.style = style
        spinner?.color = baseColor
        window.addSubview(spinner!)
        spinner!.startAnimating()
    }
    
    public static func stop() {
        guard spinner != nil else { return }
        spinner!.stopAnimating()
        spinner!.removeFromSuperview()
        spinner = nil
    }
    
    public static func update() {
        guard spinner != nil else { return }
        stop()
        start()
    }
}
