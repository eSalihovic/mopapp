//
//  ContributorCell.swift
//  MOPApp
//
//  Created by Edin Salihovic on 3/14/19.
//  Copyright © 2019 Edin Salihovic. All rights reserved.
//

import UIKit
import SDWebImage

class ContributorCell: UITableViewCell {
    
    @IBOutlet var userAvatar: CircleImage!
    @IBOutlet var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        userAvatar.image = nil
        nameLabel.text = emptyString
    }
    
    func configureCell(with contributor: Contributor) {
        userAvatar.sd_setImage(with: URL(string: contributor.avatarUrl ?? emptyString), placeholderImage: UIImage(named: avatar))
        nameLabel.text = contributor.login ?? emptyString
    }
}
