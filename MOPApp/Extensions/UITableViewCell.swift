//
//  UITableViewCell.swift
//  MOPApp
//
//  Created by Edin Salihovic on 3/18/19.
//  Copyright © 2019 Edin Salihovic. All rights reserved.
//

import UIKit

extension UITableViewCell {
    
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
